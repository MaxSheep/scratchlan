# ScratchLAN

## Linking

1. inside `server-lan` use command: `npm link`
2. inside `scratch-vm` use command: `npm link` and then `npm link scratch-lan`
3. inside `scratch-gui` use command: `npm link scratch-vm scratch-lan`
