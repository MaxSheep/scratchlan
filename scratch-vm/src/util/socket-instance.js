import openSocket from 'socket.io-client';
import config from 'scratch-lan/config';

const Socket = (function () {
    let instance;
    // eslint-disable-next-line func-style,require-jsdoc
    function SocketSingleton () {
        if (instance) {
            return instance;
        }
        if (!this.socket) {
            this.socket = openSocket(config.apiUrl);
        }
        instance = this;
    }
    SocketSingleton.clearInstance = function () {
        instance = null;
    };
    SocketSingleton.getInstance = function () {
        return instance || new SocketSingleton();
    };
    return SocketSingleton;
}());

export default Socket;
