const _axios = require('axios').default;

const SERVER_URL = 'http://localhost:8001';

const axios = _axios.create({
    baseURL: SERVER_URL,
    timeout: 10000
});

module.exports = axios;
