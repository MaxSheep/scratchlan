const ArgumentType = require('../../extension-support/argument-type');
const BlockType = require('../../extension-support/block-type');
// const log = require('../../util/log');
const socketConstants = require('scratch-lan/constants/socket-constants');
const socketInstance = require('../../util/socket-instance').default;

// Here you can add more event listeners
const eventReceivers = ['test', 'test2', 'test3'];
const variables = Object.values(socketConstants.variables);
const boolVariables = Object.values(socketConstants.boolVariables);
const serverReceivers = Object.values(socketConstants.eventReceivers);
const serverBroadcasts = Object.values(socketConstants.eventBroadcasts);

class Scratch3Lan {
    constructor (runtime) {
        this.runtime = runtime;
    }

    socket = socketInstance.getInstance().socket;
    response = {};
    reporters = new Set();

    getInfo () {
        // initialize listeners & reporters
        [...variables, ...boolVariables].forEach((reporter) => {
            this.reporter(reporter)
        })

        this.socket.on(socketConstants.informListener, (listener) => {
            this.runtime.startHats(`lan_${listener}`)
        });

        return {
            id: 'lan',
            name: 'LAN extension',
            blocks: [
                // Mapping all reporters
                // To add more variables navigate to socket-constants.js
                ...variables.map((variable) => ({
                    opcode: `report${variable}`,
                    text: variable,
                    blockType: BlockType.REPORTER,
                })),
                ...boolVariables.map((variable) => ({
                    opcode: `report${variable}`,
                    text: variable,
                    blockType: BlockType.BOOLEAN,
                })),
                {
                    opcode: `reportTrue`,
                    text: 'true',
                    blockType: BlockType.BOOLEAN,
                },
                {
                    opcode: `reportFalse`,
                    text: 'false',
                    blockType: BlockType.BOOLEAN,
                },
                {
                    id: 'set_variable',
                    opcode: 'setVariable',
                    blockType: BlockType.COMMAND,
                    text: 'set [REPORTER] to [VALUE]',
                    arguments: {
                        REPORTER: {
                            type: ArgumentType.STRING,
                            menu: 'variables'
                        },
                        VALUE: {
                            type: ArgumentType.STRING,
                            defaultValue: 0
                        }
                    }
                },
                {
                    id: 'set_variable_bool',
                    opcode: 'setVariableBool',
                    blockType: BlockType.COMMAND,
                    text: 'set [REPORTER] to [VALUE]',
                    arguments: {
                        REPORTER: {
                            type: ArgumentType.STRING,
                            menu: 'boolVariables'
                        },
                        VALUE: {
                            type: ArgumentType.BOOLEAN,
                        }
                    }
                },
                {
                    id: 'change_variable_by',
                    opcode: 'changeVariableBy',
                    blockType: BlockType.COMMAND,
                    text: 'change [REPORTER] by [VALUE]',
                    arguments: {
                        REPORTER: {
                            type: ArgumentType.STRING,
                            menu: 'variables'
                        },
                        VALUE: {
                            type: ArgumentType.STRING,
                            defaultValue: 1
                        }
                    }
                },
                {
                    id: 'show_variable',
                    opcode: 'showVariable',
                    blockType: BlockType.COMMAND,
                    text: 'show variable [REPORTER]',
                    arguments: {
                        REPORTER: {
                            type: ArgumentType.STRING,
                            menu: 'variables'
                        },
                    }
                },
                {
                    id: 'hide_variable',
                    opcode: 'hideVariable',
                    blockType: BlockType.COMMAND,
                    text: 'hide variable [REPORTER]',
                    arguments: {
                        REPORTER: {
                            type: ArgumentType.STRING,
                            menu: 'variables'
                        },
                    }
                },

                '---', // This adds spacing between blocks

                // Mapping all event listeners
                ...eventReceivers.concat(serverBroadcasts).map((code) => ({
                    opcode: code,
                    blockType: BlockType.EVENT,
                    text: `When I receive ${code}`,
                    isEdgeActivated: false,
                })),
                {
                    id: 'broadcast_message',
                    opcode: 'broadcast',
                    blockType: BlockType.COMMAND,
                    text: 'broadcast [MESSAGE]',
                    arguments: {
                        MESSAGE: {
                            type: ArgumentType.STRING,
                            menu: 'receivers'
                        }
                    }
                },
                {
                    id: 'inform_server',
                    opcode: 'informServer',
                    blockType: BlockType.COMMAND,
                    text: 'inform server [MESSAGE]',
                    arguments: {
                        MESSAGE: {
                            type: ArgumentType.STRING,
                            menu: 'serverReceivers'
                        }
                    }
                }
            ],
            menus: {
                receivers: eventReceivers,
                variables,
                boolVariables,
                serverReceivers,
            }
        };
    }

    setVariable ({ REPORTER, VALUE }) {
        this.socket.emit(socketConstants.setVariable, REPORTER, VALUE)
    }

    setVariableBool ({ REPORTER, VALUE }) {
        this.socket.emit(socketConstants.setVariable, REPORTER, VALUE)
    }

    changeVariableBy ({ REPORTER, VALUE }) {
        this.socket.emit(socketConstants.changeVariableBy, REPORTER, VALUE)
    }

    changeVisibilityMonitorBlock (blockName, value) {
        const foundFlyoutBlock = Object.values(this.runtime.flyoutBlocks._blocks).find((block) => block.id === `lan_${blockName}`)
        this.runtime.monitorBlocks.changeBlock({
            id: foundFlyoutBlock?.id,
            element: 'checkbox', // Mimic checkbox event from flyout.
            value
        });
    }

    showVariable ({ REPORTER }) {
        this.changeVisibilityMonitorBlock(`report${REPORTER}`, true)
    }

    hideVariable ({ REPORTER }) {
        this.changeVisibilityMonitorBlock(`report${REPORTER}`, false)
    }

    reporter (variableName) {
        if (this.response[variableName] !== undefined) {
            return this.response[variableName]
        }
        if (!this.reporters.has(variableName)) { // Do only once per reporter
            this.socket.on(variableName, reportedValue => {
                this.response[variableName] = reportedValue;
            });
            this.reporters.add(variableName);
        }
        return 0
    }

    reportTrue () {
        return true
    }

    reportFalse () {
        return false
    }

    broadcast ({ MESSAGE }) {
        this.socket.emit(socketConstants.broadcast, MESSAGE);
    }

    informServer ({ MESSAGE }) {
        this.socket.emit(socketConstants.informServer, MESSAGE);
    }
}

// Additional variable reporter methods
[...variables, ...boolVariables].forEach((variableName) => {
    Scratch3Lan.prototype[`report${variableName}`] = function () {
        return this.reporter(variableName)
    }
})

module.exports = Scratch3Lan;
