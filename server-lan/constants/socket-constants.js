module.exports = {
    connection: "connection",
    disconnect: "disconnect",

    addListener: 'add-listener',
    broadcast: 'broadcast',
    informServer: 'inform-server',

    setVariable: 'set-variable',
    changeVariableBy: 'change-variable-by',

    informListener: 'inform-listener',

    /* VARIABLES */
    variables: {
        team1Hp: 'team1Hp',
        team2Hp: 'team2Hp',
        playersCount: 'playersCount'
    },

    boolVariables: {
        boolTest: 'boolTest'
    },

    /* RECEIVERS */
    eventReceivers: {
        playerJoined: 'playerJoined'
    },

    /* BROADCASTS */
    eventBroadcasts: {
        draftBattle: 'draftBattle'
    },

    error: "error-message",
};
