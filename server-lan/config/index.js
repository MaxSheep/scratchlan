const config = {
    PORT: 8001,
    HOST: 'localhost',
};

module.exports = {
    ...config,
    apiUrl: `http://${config.HOST}:${config.PORT}`
};
