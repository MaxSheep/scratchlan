const constants = require("../constants/socket-constants");
const ClientsData = require("../client/data");

const clientsData = new ClientsData();
const variables = constants.variables;
const broadcasts = constants.eventBroadcasts;

module.exports = (io, socket) => {
    socket.on(constants.addListener, async (listenerName) => {
        wrapWithExceptionHandling(() => {
            if (clientsData.listeners.indexOf(listenerName) < 0) {// Check if listener already exists
                clientsData.listeners.push(listenerName);
            }
        });
    });

    socket.on(constants.broadcast, (message) => {
        wrapWithExceptionHandling(io.emit, constants.informListener, message);
    })

    socket.on(constants.informServer, (message) => {
        wrapWithExceptionHandling(messageHandlers[message]());
    })

    socket.on(constants.setVariable, (variable, value) => {
        wrapWithExceptionHandling(processSetVariableValue, variable, value);
    })

    socket.on(constants.changeVariableBy, (variable, value) => {
        wrapWithExceptionHandling(processChangeVariableValue, variable, value);
    })

    function processChangeVariableValue (variable, value) {
        const currentValue = clientsData.variables.get(variable) || 0;
        const newValue = +currentValue + +value;
        processSetVariableValue(variable, newValue);
        return newValue;
    }

    function processSetVariableValue (variable, value) {
        clientsData.variables.set(variable, value)
        io.emit(variable, value);
    }

    function wrapWithExceptionHandling(action, ...params) {
        try {
            action(...params);
        } catch (e) {
            socket.emit(constants.error, e);
        }
    }

    /* MESSAGES HANDLERS */
    function playerJoined() {
        const playersCount = processChangeVariableValue(variables.playersCount, 1);
        if (playersCount === 2) {
            io.emit(constants.informListener, broadcasts.draftBattle);
        }
    }

    const messageHandlers = {
        playerJoined: () => playerJoined()
    }
};
